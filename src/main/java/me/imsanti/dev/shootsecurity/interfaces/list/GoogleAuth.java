package me.imsanti.dev.shootsecurity.interfaces.list;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import me.imsanti.dev.shootsecurity.interfaces.Authenticator;
import me.imsanti.dev.shootsecurity.ShootSecurity;
import me.imsanti.dev.shootsecurity.managers.StaffManager;
import me.imsanti.dev.shootsecurity.objects.Staff;
import me.imsanti.dev.shootsecurity.utils.ColorUtils;
import me.imsanti.dev.shootsecurity.utils.StaffUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class GoogleAuth implements Authenticator {

    private final ShootSecurity shootSecurity;
    private final GoogleAuthenticator googleAuthenticator = new GoogleAuthenticator();
    private StaffManager staffManager;
    private StaffUtils staffUtils;
    public GoogleAuth(final ShootSecurity shootSecurity) {
        this.shootSecurity = shootSecurity;
        staffManager = new StaffManager(shootSecurity);
        staffUtils = new StaffUtils(shootSecurity);
    }

    @Override
    public void createCredentials(final Player player) {

        final GoogleAuthenticatorKey googleAuthenticatorKey = googleAuthenticator.createCredentials();
        final Staff staff = staffManager.getStaff(player.getUniqueId());
        staff.setAuthKey(googleAuthenticatorKey.getKey());
        player.sendMessage(ColorUtils.color("&eSe ha generado una key única para ti. Introducela en Google Auth"));
        player.sendMessage(ColorUtils.color("&bTu clave secreta es " + googleAuthenticatorKey.getKey()));

        final FileConfiguration config = staffUtils.getStaffConfig(player.getName());
        config.set("secret-key", googleAuthenticatorKey.getKey());
        shootSecurity.getConfigManager().saveConfigFile(staffUtils.getStaffFile(player), config);
    }

    @Override
    public boolean isValidCode(int code, Staff staff) {
        return googleAuthenticator.authorize(staff.getAuthKey(), code);
    }

    @Override
    public void resetCredentials() {
        //todo: reset credentials!
    }

    @Override
    public void invalidCodeMethod(Player player) {
        new BukkitRunnable() {

            @Override
            public void run() {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "Kick " + player.getName() + " Security reasons!");
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ipban " + player.getName() + " Security reasons!");
            }
        }.runTaskLater(shootSecurity, 1L);

    }
}
