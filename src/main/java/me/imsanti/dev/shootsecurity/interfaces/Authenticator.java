package me.imsanti.dev.shootsecurity.interfaces;

import me.imsanti.dev.shootsecurity.objects.Staff;
import org.bukkit.entity.Player;

public interface Authenticator {

    void createCredentials(final Player player);
    boolean isValidCode(final int code, final Staff staff);
     void resetCredentials();
     void invalidCodeMethod(final Player player);
}
