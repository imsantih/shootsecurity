package me.imsanti.dev.shootsecurity.commands;

import me.imsanti.dev.shootsecurity.ShootSecurity;
import me.imsanti.dev.shootsecurity.utils.ColorUtils;
import me.imsanti.dev.shootsecurity.utils.StaffUtils;
import me.imsanti.dev.shootsecurity.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class RegisterStaffCommand implements CommandExecutor {

    private final ShootSecurity shootSecurity;

    public RegisterStaffCommand(final ShootSecurity shootSecurity) {
        this.shootSecurity = shootSecurity;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if(!(sender instanceof Player)) {
            Bukkit.getConsoleSender().sendMessage(ColorUtils.color("&cThe console can't use this command!"));
            return true;
        }

        final Player player = (Player) sender;

        if(!StaffUtils.isAllowedToRegister(player)) {
            player.sendMessage(ColorUtils.color("&cPermisos insuficientes."));
            return true;
        }

        if(!Utils.isValidPlayer(args[0])) {
            player.sendMessage(ColorUtils.color("&cThe player " + args[0] + " don't exists."));
            return true;
        }

        final Player target = Bukkit.getPlayer(args[0]);
        if(target == null) return true;
        shootSecurity.getStaffManager().createStaff(target);
        return true;
    }
}
