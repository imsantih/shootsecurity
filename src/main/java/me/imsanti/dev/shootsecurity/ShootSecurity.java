package me.imsanti.dev.shootsecurity;

import me.imsanti.dev.shootsecurity.commands.RegisterStaffCommand;
import me.imsanti.dev.shootsecurity.configs.ConfigManager;
import me.imsanti.dev.shootsecurity.listeners.BlocksListener;
import me.imsanti.dev.shootsecurity.listeners.PlayerListener;
import me.imsanti.dev.shootsecurity.managers.StaffManager;
import me.imsanti.dev.shootsecurity.objects.Staff;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public final class ShootSecurity extends JavaPlugin {

    private final StaffManager staffManager = new StaffManager(this);
    private final ConfigManager configManager = new ConfigManager(this);
    private final Map<UUID, Staff> loadedStaffs = new HashMap<>();

    @Override
    public void onEnable() {
        registerCommands();
        Bukkit.getConsoleSender().sendMessage(String.valueOf(loadedStaffs.size()));
        staffManager.loadAllStaffs();
        registerEvents();
        // Plugin startup logic

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private void registerCommands() {
        getCommand("registerstaff").setExecutor(new RegisterStaffCommand(this));
    }
    private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);
        Bukkit.getPluginManager().registerEvents(new BlocksListener(this), this);
    }

    public StaffManager getStaffManager() {
        return staffManager;
    }

    public Map<UUID, Staff> getLoadedStaffs() {
        return loadedStaffs;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }
}
