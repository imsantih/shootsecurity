package me.imsanti.dev.shootsecurity.utils;

import me.imsanti.dev.shootsecurity.ShootSecurity;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import javax.swing.text.PlainDocument;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static boolean isValidPlayer(final String playerName) {
        return Bukkit.getPlayer(playerName) != null && Bukkit.getPlayer(playerName).isOnline();
    }

    public static String getRank(final Player player) {
        String rankName = "";
        if(player.hasPermission("group.helper")) {
            rankName = "helper";
        }else if(player.hasPermission("group.mod")) {
                rankName = "mod";
        }else if(player.hasPermission("group.srmod")) {
            rankName = "srmod";
        }else if(player.hasPermission("group.jradmin")) {
            rankName = "jradmin";
        }else if(player.hasPermission("group.admin")) {
            rankName = "admin";
        }else if(player.hasPermission("group.sradmin")) {
            rankName = "sradmin";
        }else if(player.hasPermission("group.chiefadmin")) {
            rankName = "chiefadmin";
        }else if(player.hasPermission("group.developer")) {
            rankName = "developer";
        }else if(player.hasPermission("group.coowner")) {
                rankName = "coowner";
        }else if(player.hasPermission("group.coowner")) {
            rankName = "owner";
        }
        return rankName;
    }
}
