package me.imsanti.dev.shootsecurity.utils;

import org.bukkit.ChatColor;

public class ColorUtils {

    public static String color(final String text) {
        return ChatColor.translateAlternateColorCodes('&', text);
    }
}
