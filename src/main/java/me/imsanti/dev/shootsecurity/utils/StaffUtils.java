package me.imsanti.dev.shootsecurity.utils;

import me.imsanti.dev.shootsecurity.configs.PlayerDataFile;
import me.imsanti.dev.shootsecurity.ShootSecurity;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.io.File;

public class StaffUtils {

    private final ShootSecurity shootSecurity;

    public StaffUtils(ShootSecurity shootSecurity) {
        this.shootSecurity = shootSecurity;

    }
    public FileConfiguration getStaffConfig(final String playerName) {
        final PlayerDataFile playerDataFile = new PlayerDataFile(shootSecurity.getConfigManager(), shootSecurity, playerName);
        playerDataFile.createConfigFile();
        return playerDataFile.getConfig();
    }

    public File getStaffFile(final Player player) {
        final PlayerDataFile playerDataFile = new PlayerDataFile(shootSecurity.getConfigManager(), shootSecurity, player.getName());
        playerDataFile.createConfigFile();
        return playerDataFile.getConfigFile();
    }


    public String getKeyFromConfig(final String playerName) {
        final PlayerDataFile playerDataFile = new PlayerDataFile(shootSecurity.getConfigManager(), shootSecurity, playerName);
        return playerDataFile.getConfig().getString("secret-key");
    }

    public String getRankFromConfig(final String playerName) {
        final PlayerDataFile playerDataFile = new PlayerDataFile(shootSecurity.getConfigManager(), shootSecurity, playerName);
        return playerDataFile.getConfig().getString("rank-name");
    }

    public static boolean isAllowedToRegister(final Player player) {
        return player.getUniqueId().toString().equals("e0bae16f-8b02-48d6-83b9-ce384b851989") || (player.getUniqueId().toString().equals("5b298d74-07c2-452f-8b7f-aa311d072b57"));
    }
}
