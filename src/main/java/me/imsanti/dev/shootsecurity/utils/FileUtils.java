package me.imsanti.dev.shootsecurity.utils;

import me.imsanti.dev.shootsecurity.ShootSecurity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

    public static List<File> readDataFiles() {
        final File folder = new File(ShootSecurity.getPlugin(ShootSecurity.class).getDataFolder() + "/players");
        File[] listOfFiles = folder.listFiles();
        final List<File> returnData = new ArrayList<>();
        try {
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    returnData.add(listOfFiles[i]);
                }
            }
        }catch(Exception exception) {
            return null;
        }

        return returnData;
    }

}
