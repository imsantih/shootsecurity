package me.imsanti.dev.shootsecurity.managers;

import me.imsanti.dev.shootsecurity.ShootSecurity;
import me.imsanti.dev.shootsecurity.interfaces.list.GoogleAuth;
import me.imsanti.dev.shootsecurity.objects.Staff;
import me.imsanti.dev.shootsecurity.utils.FileUtils;
import me.imsanti.dev.shootsecurity.utils.StaffUtils;
import me.imsanti.dev.shootsecurity.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.Map;
import java.util.UUID;

public class StaffManager {

    private final ShootSecurity shootSecurity;
    private StaffUtils staffUtils;

    public StaffManager(final ShootSecurity shootSecurity) {
        this.shootSecurity = shootSecurity;
        staffUtils = new StaffUtils(shootSecurity);
    }
    public boolean isStaff(final String uuid) {
        if(Bukkit.getPlayer(UUID.fromString(uuid)) == null) return false;
        return shootSecurity.getLoadedStaffs().containsKey(UUID.fromString(uuid));
    }

    public boolean ipMatch(final String ip, final UUID uuid) {
        if(!shootSecurity.getLoadedStaffs().containsKey(uuid)) return false;

        final Staff staff = shootSecurity.getLoadedStaffs().get(uuid);
        return ip.contains(staff.getIp());
    }

    public Staff getStaff(final UUID uuid) {
        return shootSecurity.getLoadedStaffs().get(uuid);
    }

    public void loadStaff(final UUID uuid, final String playerName, final String ip) {
        shootSecurity.getLoadedStaffs().put(uuid, new Staff(ip, uuid, staffUtils.getKeyFromConfig(playerName), staffUtils.getRankFromConfig(playerName)));
    }

    public void loadAllStaffs() {
        if(FileUtils.readDataFiles() == null) return;

        for(final File file : FileUtils.readDataFiles()) {
            final FileConfiguration staffConfig = staffUtils.getStaffConfig(file.getName().replaceAll(".yml", ""));
            loadStaff(UUID.fromString(staffConfig.getString("uuid")), file.getName().replaceAll(".yml", ""), staffConfig.getString("ip"));
            Bukkit.getConsoleSender().sendMessage("Staff loaded!");
    }
}

    public void createStaff(final Player player) {
        loadStaff(player.getUniqueId(), player.getName(), player.getAddress().getAddress().toString());
        staffUtils.getStaffConfig(player.getName());
        final GoogleAuth googleAuth = new GoogleAuth(shootSecurity);
        googleAuth.createCredentials(player);
        final FileConfiguration config = staffUtils.getStaffConfig(player.getName());
        config.set("uuid", player.getUniqueId().toString());
        config.set("ip", player.getAddress().getAddress().toString());
        config.set("rank-name", Utils.getRank(player));
        config.set("is-op", player.isOp());
        shootSecurity.getConfigManager().saveConfigFile(staffUtils.getStaffFile(player), config);
    }

    public void disablePermissions(final Player player) {
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + player.getName() + " parent set default");
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "deop " + player.getName());
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + player.getName() + " permission unset *");

    }

    public void enablePermissions(final Player player) {
        new BukkitRunnable() {
            @Override
            public void run() {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + player.getName() + " parent set " + staffUtils.getRankFromConfig(player.getName()));
                if(staffUtils.getStaffConfig(player.getName()).getBoolean("is-op")) {
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "op " + player.getName());
                }
            }
        }.runTaskLater(shootSecurity, 2L);
    }

    public Map<UUID, Staff> getStaffs() {
        return shootSecurity.getLoadedStaffs();
    }
}
