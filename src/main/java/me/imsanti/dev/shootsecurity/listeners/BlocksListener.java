package me.imsanti.dev.shootsecurity.listeners;

import me.imsanti.dev.shootsecurity.ShootSecurity;
import me.imsanti.dev.shootsecurity.objects.Staff;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;

public class BlocksListener implements Listener {

    private final ShootSecurity shootSecurity;

    public BlocksListener(final ShootSecurity shootSecurity) {
        this.shootSecurity = shootSecurity;
    }

    @EventHandler
    private void handleBlockPlace(final BlockPlaceEvent event) {
        if(!shootSecurity.getStaffManager().isStaff(event.getPlayer().getUniqueId().toString())) return;

        final Staff staff = shootSecurity.getStaffManager().getStaff(event.getPlayer().getUniqueId());
        if(!staff.isAuthenticated()) event.setCancelled(true);
    }

    @EventHandler
    private void handleBlockBreak(final BlockBreakEvent event) {
        if(!shootSecurity.getStaffManager().isStaff(event.getPlayer().getUniqueId().toString())) return;

        final Staff staff = shootSecurity.getStaffManager().getStaff(event.getPlayer().getUniqueId());
        if(!staff.isAuthenticated()) event.setCancelled(true);
    }

    @EventHandler
    private void handleCommand(final PlayerCommandPreprocessEvent event) {
        if(!shootSecurity.getStaffManager().isStaff(event.getPlayer().getUniqueId().toString())) return;

        final Staff staff = shootSecurity.getStaffManager().getStaff(event.getPlayer().getUniqueId());
        if(!staff.isAuthenticated()) event.setCancelled(true);
    }

    @EventHandler
    private void handleCommand(final EntityDamageEvent event) {
        if(!(event.getEntity().getType() == EntityType.PLAYER)) return;
        final Player player = (Player)  event.getEntity();

        if(!shootSecurity.getStaffManager().isStaff(player.getUniqueId().toString())) return;

        final Staff staff = shootSecurity.getStaffManager().getStaff(player.getUniqueId());
        if(!staff.isAuthenticated()) event.setCancelled(true);
    }

    @EventHandler
    private void handleGamemodeChange(final PlayerGameModeChangeEvent event) {
        final Player player = event.getPlayer();

        if(!shootSecurity.getStaffManager().isStaff(player.getUniqueId().toString())) return;

        final Staff staff = shootSecurity.getStaffManager().getStaff(player.getUniqueId());
        if(!staff.isAuthenticated()) event.setCancelled(true);
    }

    @EventHandler
    private void handleWorldChange(final PlayerChangedWorldEvent event) {
        final Player player = event.getPlayer();

        if(!shootSecurity.getStaffManager().isStaff(player.getUniqueId().toString())) return;

        final Staff staff = shootSecurity.getStaffManager().getStaff(player.getUniqueId());
        if(!staff.isAuthenticated()) Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "kick " + event.getPlayer().getName());
    }




}
