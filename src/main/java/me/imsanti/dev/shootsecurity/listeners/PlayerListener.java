package me.imsanti.dev.shootsecurity.listeners;

import me.imsanti.dev.shootsecurity.ShootSecurity;
import me.imsanti.dev.shootsecurity.interfaces.list.GoogleAuth;
import me.imsanti.dev.shootsecurity.objects.Staff;
import me.imsanti.dev.shootsecurity.utils.ColorUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

    private final ShootSecurity shootSecurity;

    public PlayerListener(final ShootSecurity shootSecurity) {
        this.shootSecurity = shootSecurity;
    }

    @EventHandler
    private void handleConnect(final PlayerJoinEvent event) {
        if(!shootSecurity.getStaffManager().isStaff(String.valueOf(event.getPlayer().getUniqueId()))) return;
        final Staff staff = shootSecurity.getStaffManager().getStaff(event.getPlayer().getUniqueId());
        if(!shootSecurity.getStaffManager().ipMatch(event.getPlayer().getAddress().getAddress().toString(), event.getPlayer().getUniqueId())) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "kick " + event.getPlayer().getName() + " Security reasons");
            Bukkit.getConsoleSender().sendMessage(staff.getIp());
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ipban " + event.getPlayer().getName() + " Security reasons");
        }
    }

    @EventHandler
    private void handleGoogleCode(final PlayerJoinEvent event) {
        if(!shootSecurity.getStaffManager().isStaff(String.valueOf(event.getPlayer().getUniqueId()))) return;
        final Staff staff = shootSecurity.getStaffManager().getStaff(event.getPlayer().getUniqueId());
        final GoogleAuth googleAuth = new GoogleAuth(shootSecurity);

        if(staff.getAuthKey() == null || staff.getAuthKey().equals("")) {
            googleAuth.createCredentials(event.getPlayer());
            staff.setAuthenticated(false);
        }else {
            staff.setAuthenticated(false);
            event.getPlayer().sendMessage(ColorUtils.color("&cPor favor, escribe en un mensaje tu clave que te sale en la App de Google"));
        }
    }

    @EventHandler
    private void handlePlayerChat(final AsyncPlayerChatEvent event) {
        if(!shootSecurity.getStaffManager().isStaff(String.valueOf(event.getPlayer().getUniqueId()))) return;
        final Staff staff = shootSecurity.getStaffManager().getStaff(event.getPlayer().getUniqueId());
        if(staff.isAuthenticated()) return;

        event.setCancelled(true);
        final GoogleAuth googleAuth = new GoogleAuth(shootSecurity);
        final String code = event.getMessage();

        try {
            if(googleAuth.isValidCode(Integer.parseInt(code), staff)) {
                event.getPlayer().sendMessage(ColorUtils.color("&aTe has logueado con exito!"));
                staff.setAuthenticated(true);
                shootSecurity.getStaffManager().enablePermissions(event.getPlayer());
                event.getPlayer().sendMessage(ColorUtils.color("&eAhora tienes los permisos de staff activos."));
                event.setCancelled(true);
                return;
            }
        }catch(Exception exception) {
            googleAuth.invalidCodeMethod(event.getPlayer());
            staff.setAuthenticated(false);
            event.setCancelled(true);
            return;
        }

        googleAuth.invalidCodeMethod(event.getPlayer());
        staff.setAuthenticated(false);
        event.setCancelled(true);
    }

    @EventHandler
    private void handleDisconnect(final PlayerQuitEvent event) {
        if(!shootSecurity.getStaffManager().isStaff(event.getPlayer().getUniqueId().toString())) return;
        shootSecurity.getStaffManager().disablePermissions(event.getPlayer());
    }

}
