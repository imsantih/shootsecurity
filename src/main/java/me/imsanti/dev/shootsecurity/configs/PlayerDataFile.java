package me.imsanti.dev.shootsecurity.configs;

import me.imsanti.dev.shootsecurity.ShootSecurity;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class PlayerDataFile {

    private final ShootSecurity shootSecurity;
    private final String playerName;
    private final ConfigManager configManager;

    public PlayerDataFile(ConfigManager configManager, final ShootSecurity shootSecurity, final String playerName) {
        this.configManager = configManager;
        this.shootSecurity = shootSecurity;
        this.playerName = playerName;
    }

    public boolean createConfigFile() {
        if(configManager.createConfigFile(String.valueOf(shootSecurity.getDataFolder() + "/players"), playerName, false)) return true;

        return false;
    }

    public void saveConfigFile(FileConfiguration config) {
        configManager.saveConfigFile(getConfigFile(), config);
    }

    public FileConfiguration getConfig() {
        return configManager.getConfigFromFile(String.valueOf(shootSecurity.getDataFolder() + "/players"), playerName);
    }

    public String getPlayerName() {
        return playerName;
    }

    public File getConfigFile() {
        return new File(shootSecurity.getDataFolder() + "/players", playerName + ".yml");
    }

    public boolean fileExists() {
        return new File(shootSecurity.getDataFolder() + "/players", playerName + ".yml").exists();
    }
}
