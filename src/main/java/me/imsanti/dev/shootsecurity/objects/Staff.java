package me.imsanti.dev.shootsecurity.objects;

import java.util.UUID;

public class Staff {

    private final String ip;
    private final UUID uuid;
    private String authKey;
    private final String rankName;
    private boolean isAuthenticated;

    public Staff(final String ip, final UUID uuid, final String authKey, final String rankName) {
        this.ip = ip;
        this.uuid = uuid;
        this.authKey = authKey;
        this.rankName = rankName;
    }

    public String getIp() {
        return ip;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public void setAuthenticated(boolean authenticated) {
        isAuthenticated = authenticated;
    }

    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    public String getRankName() {
        return rankName;
    }
}
